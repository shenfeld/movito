package com.shenfeld.endlessscrollview.presentation

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.shenfeld.endlessscrollview.domain.repos.MovieDataSource
import com.shenfeld.endlessscrollview.presentation.adapter.MovieModel

class MovieSourceFactory(
    private val moviesDataSource: MovieDataSource,
    private val initialLoading: MutableLiveData<Boolean>,
    private val pagination: MutableLiveData<Boolean>
) : DataSource.Factory<Int, MovieModel>() {
    override fun create(): DataSource<Int, MovieModel> {
        return MoviesPagedDataSource(moviesDataSource, initialLoading, pagination)
    }
}