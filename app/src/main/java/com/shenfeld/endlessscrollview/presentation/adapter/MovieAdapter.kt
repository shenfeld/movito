package com.shenfeld.endlessscrollview.presentation.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.shenfeld.endlessscrollview.R

class MovieAdapter(
    context: Context,
    diffCallback: MoviesDiffCallback
) : PaginationAdapter<MovieModel>(context, diffCallback) {

    class MovieViewHolder(v: View) : RecyclerView.ViewHolder(v), Bindable<MovieModel> {
        private var tvTitleMovie = v.findViewById<TextView>(R.id.tvTitleMovie)
        private var tvOverwriteMovie = v.findViewById<TextView>(R.id.tvOverwriteMovie)
        private var tvDateMovie = v.findViewById<TextView>(R.id.tvDateMovie)
        private var tvRatingMovie = v.findViewById<TextView>(R.id.tvRatingMovie)
        override fun bind(item: MovieModel) {
            tvTitleMovie.text = item.title
            tvOverwriteMovie.text = item.overview
            tvDateMovie.text = item.releaseDate
            tvRatingMovie.text = item.voteAverage.toString()
        }
    }

    override fun getViewTypeForItem(pos: Int): Int {
        return R.layout.rv_item
    }

    override fun onCreateViewHolderForItem(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        val view = layoutInflater.inflate(R.layout.rv_item, parent, false)
        return MovieViewHolder(view)
    }

    override fun onBindViewHolderForItem(holder: RecyclerView.ViewHolder, pos: Int) {
        val movie = getItem(pos)
        if (movie != null && holder is MovieViewHolder) {
            holder.bind(movie)
        }
    }

    interface Bindable<T> {
        fun bind(item: T)
    }
}