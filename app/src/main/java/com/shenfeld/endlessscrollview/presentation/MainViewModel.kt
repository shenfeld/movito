package com.shenfeld.endlessscrollview.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.shenfeld.endlessscrollview.domain.repos.MovieDataSource
import com.shenfeld.endlessscrollview.domain.usecases.GetMoviesPageInfoUseCase
import com.shenfeld.endlessscrollview.presentation.adapter.MovieModel
import com.shenfeld.endlessscrollview.presentation.utils.ResourcesManager
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.util.concurrent.Executors

class MainViewModel : ViewModel(), KoinComponent {
    private val resourcesManager: ResourcesManager by inject()
    private val getMoviesPageInfoUseCase: GetMoviesPageInfoUseCase by inject()
    private val movieDataSource: MovieDataSource by inject()
    val moviePageLiveData = MutableLiveData<List<MovieModel>>()
    val moviesPagedList = MutableLiveData<PagedList<MovieModel>>()
    val initialLoadingLiveData = MutableLiveData<Boolean>()
    val paginationLiveData = MutableLiveData<Boolean>()

    private val moviesSourceFactory = MovieSourceFactory(
        movieDataSource,
        initialLoadingLiveData,
        paginationLiveData
    )

    private val config = PagedList.Config.Builder()
        .setEnablePlaceholders(false)
        .setPrefetchDistance(PREFETCH_DISTANCE)
        .build()

    private val movies = LivePagedListBuilder(moviesSourceFactory, config)
        .build()

    companion object {
        private const val PREFETCH_DISTANCE = 1
    }

    private val newsObserver = Observer<PagedList<MovieModel>> {
        moviesPagedList.postValue(it)
    }

    fun onViewCreated() {
        movies.observeForever(newsObserver)
    }

    fun onViewDestroyed() {
        movies.removeObserver(newsObserver)
    }
}

