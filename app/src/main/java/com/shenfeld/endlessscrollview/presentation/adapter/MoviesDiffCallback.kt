package com.shenfeld.endlessscrollview.presentation.adapter

import androidx.recyclerview.widget.DiffUtil

class MoviesDiffCallback : DiffUtil.ItemCallback<MovieModel>() {

    override fun areItemsTheSame(oldItem: MovieModel, newItem: MovieModel): Boolean {
        return oldItem.title == newItem.title
    }

    override fun areContentsTheSame(oldItem: MovieModel, newItem: MovieModel): Boolean {
        return oldItem.overview == newItem.overview
    }
}
