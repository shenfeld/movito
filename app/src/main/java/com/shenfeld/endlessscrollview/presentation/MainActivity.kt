package com.shenfeld.endlessscrollview.presentation

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.AlphaAnimation
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.shenfeld.endlessscrollview.ANIMATION_DURATION
import com.shenfeld.endlessscrollview.R
import com.shenfeld.endlessscrollview.presentation.adapter.MovieAdapter
import com.shenfeld.endlessscrollview.presentation.adapter.MoviesDiffCallback
import com.shenfeld.endlessscrollview.presentation.adapter.RecyclerItemClickListener
import com.shenfeld.endlessscrollview.presentation.sheet_fragment.CustomBottomSheetDialogFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_loading.*
import org.koin.android.ext.android.inject

@Suppress("ControlFlowWithEmptyBody")
class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by inject()
    private var adapter: MovieAdapter? = null
    private var isLoading = false
    private var page = 2
    private val newsDiffCallback = MoviesDiffCallback()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupView()
        setupObservers()
        viewModel.onViewCreated()
    }

    private fun setupObservers() {
        with(viewModel) {
            moviesPagedList.observe(this@MainActivity, {
                adapter?.submitList(it)
                Log.e("MainActivity", "moviesPagedList")
            })

            initialLoadingLiveData.observe(this@MainActivity, {
                if (it) {
                    showLoading()
                    Log.e("MainActivity", "showLoading()")
                } else {
                    showMain()
                    Log.e("MainActivity", "showMain()")
                }
            })

            paginationLiveData.observe(this@MainActivity, {
                if (it) {
                    showPagination()
                    Log.e("MainActivity", "showPagination()")
                } else {
                    hidePagination()
                    Log.e("MainActivity", "hidePagination()")
                }
            })
        }
    }

    private fun setupView() {
        adapter = MovieAdapter(context = applicationContext, newsDiffCallback)
        rvMain.layoutManager = LinearLayoutManager(this)
        rvMain.adapter = adapter

        rvMain.addOnItemTouchListener(
            RecyclerItemClickListener(
                this,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        view.setOnClickListener {
                            CustomBottomSheetDialogFragment().apply {
                                show(supportFragmentManager, CustomBottomSheetDialogFragment.TAG)
                            }
                        }
                    }
                })
        )
    }

    private fun hidePagination() {
        adapter?.hidePagination()
    }

    private fun showPagination() {
        adapter?.showPagination()
    }

    private fun showMain() {
        if (rvMain.isGone) {
            val alphaAnimation = AlphaAnimation(0f, 1f).apply {
                duration = ANIMATION_DURATION
            }
            pbLoading.animation = alphaAnimation
            pbLoading.visibility = View.VISIBLE
        }
        if (pbLoading.isVisible) {
            val alphaAnimation = AlphaAnimation(1f, 0f).apply {
                duration = ANIMATION_DURATION
            }
            rvMain.animation = alphaAnimation
            rvMain.visibility = View.GONE
        }
    }

    private fun showLoading() {
        if (pbLoading.visibility != View.VISIBLE) {
            val alphaAnimation = AlphaAnimation(0f, 1f).apply {
                duration = ANIMATION_DURATION
            }
            pbLoading.animation = alphaAnimation
            pbLoading.visibility = View.VISIBLE
        }
        if (rvMain.visibility != View.GONE) {
            val alphaAnimation = AlphaAnimation(1f, 0f).apply {
                duration = ANIMATION_DURATION
            }
            rvMain.animation = alphaAnimation
            rvMain.visibility = View.GONE
        }
    }

    override fun onDestroy() {
        viewModel.onViewDestroyed()
        super.onDestroy()
    }
}