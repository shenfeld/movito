package com.shenfeld.endlessscrollview.presentation.base

import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<ModelT, ViewHolderT : RecyclerView.ViewHolder> :
    RecyclerView.Adapter<ViewHolderT>() {

    val items: MutableList<ModelT> = mutableListOf()

    override fun getItemCount(): Int {
        return items.size
    }

    fun <T : ModelT> updateAllItems(items: List<T>) {
        this.items.clear()
        this.items.addAll(items)
        this.notifyDataSetChanged()
    }
}
