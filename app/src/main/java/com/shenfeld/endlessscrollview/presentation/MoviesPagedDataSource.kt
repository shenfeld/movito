package com.shenfeld.endlessscrollview.presentation

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.shenfeld.endlessscrollview.domain.models.Movie
import com.shenfeld.endlessscrollview.domain.repos.MovieDataSource
import com.shenfeld.endlessscrollview.presentation.adapter.MovieModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MoviesPagedDataSource(
    private val moviesDataSource: MovieDataSource,
    initialLoading: MutableLiveData<Boolean>,
    pagination: MutableLiveData<Boolean>
) : PaginationDataSource<Int, MovieModel>(initialLoading, pagination) {

    private var page: Int = 1

    override fun loadInitialData(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, MovieModel>
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            val movies = getMovies(page = page)
            val newMovies = toMovieModel(movies)
            page++
            callback.onResult(newMovies, null, page)
        }
        Log.e("MoviesPagedDataSource", "loadInitialData")
    }

    override fun loadDataAfter(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, MovieModel>
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            val movies = getMovies(page = page)
            val newMovies = toMovieModel(movies)
            page++
            callback.onResult(newMovies, page)
        }
        Log.e("MoviesPagedDataSource", "loadDataAfter")
    }

    private fun toMovieModel(movies: List<Movie>): List<MovieModel> {
        return movies.map {
            MovieModel(
                title = it.title,
                overview = it.overview,
                releaseDate = it.releaseDate,
                voteAverage = it.voteAverage
            )
        }
    }

    private suspend fun getMovies(page: Int): List<Movie> {
        return moviesDataSource.getPageMovies(page)
    }
}