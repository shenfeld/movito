package com.shenfeld.endlessscrollview.presentation

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource

abstract class PaginationDataSource<K, V>(
    private val initialLoading: MutableLiveData<Boolean>,
    private val pagination: MutableLiveData<Boolean>
) : PageKeyedDataSource<K, V>() {

    protected abstract fun loadInitialData(
        params: LoadInitialParams<K>,
        callback: LoadInitialCallback<K, V>
    )

    protected abstract fun loadDataAfter(
        params: LoadParams<K>,
        callback: LoadCallback<K, V>
    )

    override fun loadInitial(params: LoadInitialParams<K>, callback: LoadInitialCallback<K, V>) {
        Log.e("PaginationDataSource", "initial loading true")
        initialLoading.postValue(true)
        loadInitialData(params = params, callback = callback)
        Log.e("PaginationDataSource", "loadInitial")
        initialLoading.postValue(false)
        Log.e("PaginationDataSource", "initial loading false")
    }

    override fun loadAfter(params: LoadParams<K>, callback: LoadCallback<K, V>) {
        Log.e("PaginationDataSource", "load after true")
        pagination.postValue(true)
        loadDataAfter(params = params, callback = callback)
        Log.e("PaginationDataSource", "loadAfter done")
        pagination.postValue(false)
        Log.e("PaginationDataSource", "load after false")
    }

    override fun loadBefore(params: LoadParams<K>, callback: LoadCallback<K, V>) {
        // not used
    }
}