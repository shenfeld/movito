package com.shenfeld.endlessscrollview.presentation.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.shenfeld.endlessscrollview.R

abstract class PaginationAdapter<T>(
    context: Context,
    diffCallback: DiffUtil.ItemCallback<T>
) : PagedListAdapter<T, RecyclerView.ViewHolder>(diffCallback) {

    private var paginationStatus = PaginationStatus.HIDDEN
    protected val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    protected abstract fun getViewTypeForItem(pos: Int): Int
    protected abstract fun onCreateViewHolderForItem(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder

    protected abstract fun onBindViewHolderForItem(
        holder: RecyclerView.ViewHolder,
        pos: Int
    )

    override fun getItemViewType(position: Int): Int {
        return if (position == itemCount - 1 && paginationStatus == PaginationStatus.LOADING) {
            Log.e("PaginationAdapter", "getItemViewType условие if сработало !")
            R.layout.item_loading
        } else {
            Log.e("PaginationAdapter", "R.layout.rv_item")
            getViewTypeForItem(position)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == R.layout.item_loading) {
            val view = layoutInflater.inflate(R.layout.item_loading, parent, false)
            PaginationItemViewHolder(view)
        } else {
            onCreateViewHolderForItem(parent, viewType)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val itemType = getItemViewType(position)
        if (itemType != R.layout.item_loading) {
            onBindViewHolderForItem(holder, position)
        }
    }

    override fun getItemCount(): Int {
        var count = super.getItemCount()
        if (paginationStatus == PaginationStatus.LOADING) {
            count++
            Log.e("PaginationAdapter", "getItemCount() условие if")
        }
        return count
    }

    fun showPagination() {
        if (paginationStatus != PaginationStatus.LOADING) {
            paginationStatus = PaginationStatus.LOADING
            notifyItemInserted(super.getItemCount())
            Log.e("PaginationAdapter", "showPagination() условие if")
        }
        Log.e("PaginationAdapter", "showPagination() Item's count - ${super.getItemCount()}")
    }

    fun hidePagination() {
        if (paginationStatus != PaginationStatus.HIDDEN) {
            paginationStatus = PaginationStatus.HIDDEN
            notifyItemRemoved(super.getItemCount())
            Log.e("PaginationAdapter", "hidePagination() условие if")
        }
        Log.e("PaginationAdapter", "hidePagination() Item's count - ${super.getItemCount()}")
    }

    private class PaginationItemViewHolder(view: View) : RecyclerView.ViewHolder(view)

    enum class PaginationStatus {
        HIDDEN, LOADING
    }
}


