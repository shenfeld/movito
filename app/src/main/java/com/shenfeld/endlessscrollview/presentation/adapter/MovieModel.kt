package com.shenfeld.endlessscrollview.presentation.adapter

data class MovieModel(
    val title: String,
    val overview: String,
    val releaseDate: String,
    val voteAverage: Double
)