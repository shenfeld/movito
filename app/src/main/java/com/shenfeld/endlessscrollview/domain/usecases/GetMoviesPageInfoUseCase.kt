package com.shenfeld.endlessscrollview.domain.usecases

import com.shenfeld.endlessscrollview.domain.models.Movie

interface GetMoviesPageInfoUseCase {
    suspend fun execute(page: Int): List<Movie>
}