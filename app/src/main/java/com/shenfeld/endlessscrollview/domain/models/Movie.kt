package com.shenfeld.endlessscrollview.domain.models

import com.google.gson.annotations.SerializedName

data class Movie(
    val title: String,
    val overview: String,
    val releaseDate: String,
    val voteAverage: Double
)