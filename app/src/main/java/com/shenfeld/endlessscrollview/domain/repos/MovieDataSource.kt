package com.shenfeld.endlessscrollview.domain.repos

import com.shenfeld.endlessscrollview.domain.models.Movie

interface MovieDataSource {
    suspend fun getPageMovies(page: Int): List<Movie>
}