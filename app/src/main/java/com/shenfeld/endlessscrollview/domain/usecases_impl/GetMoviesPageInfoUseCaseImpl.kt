package com.shenfeld.endlessscrollview.domain.usecases_impl

import com.shenfeld.endlessscrollview.domain.models.Movie
import com.shenfeld.endlessscrollview.domain.repos.MovieDataSource
import com.shenfeld.endlessscrollview.domain.usecases.GetMoviesPageInfoUseCase

class GetMoviesPageInfoUseCaseImpl(
    private val movieDataSource: MovieDataSource
) : GetMoviesPageInfoUseCase {
    override suspend fun execute(page: Int): List<Movie> {
        return movieDataSource.getPageMovies(page = page)
    }
}