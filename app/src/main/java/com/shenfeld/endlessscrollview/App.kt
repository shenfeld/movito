package com.shenfeld.endlessscrollview

import android.app.Application
import com.shenfeld.endlessscrollview.di.dataModule
import com.shenfeld.endlessscrollview.di.domainModule
import com.shenfeld.endlessscrollview.di.presentationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(presentationModule, domainModule, dataModule)
        }
    }
}