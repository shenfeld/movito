package com.shenfeld.endlessscrollview

enum class State {
    DONE, LOADING, ERROR
}