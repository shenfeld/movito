package com.shenfeld.endlessscrollview.data.repos

import com.shenfeld.endlessscrollview.data.MovieAPI
import com.shenfeld.endlessscrollview.domain.models.Movie
import com.shenfeld.endlessscrollview.domain.repos.MovieDataSource

class MovieRepository(
    private val movieAPI: MovieAPI
) : MovieDataSource {

    override suspend fun getPageMovies(page: Int): List<Movie> {
        val movies = movieAPI.getPageMovies(page = page).results
        return movies.map {
            Movie(
                title = it.title,
                overview = it.overview,
                releaseDate = it.releaseDate,
                voteAverage = it.voteAverage
            )
        }
    }
}

//    var state: MutableLiveData<State> = MutableLiveData()
//    private fun toMovieList(movies: List<Results>) {
//        movies.map {
//            Movie(
//                title = it.title,
//                overview = it.overview,
//                releaseDate = it.releaseDate,
//                voteAverage = it.voteAverage
//            )
//        }
//    }
//    private fun updateState(state: State) {
//        this.state.postValue(state)
//    }
//    override fun loadInitial(
//        params: LoadInitialParams<Int>,
//        callback: LoadInitialCallback<Int, Results>
//    ) {
//        updateState(State.LOADING)
//        GlobalScope.launch(Dispatchers.IO) {
//            val movies = movieAPI.getPageMovies(page = 1).results
//            if (movies.isNotEmpty()) {
//                updateState(State.DONE)
//                toMovieList(movies)
//                callback.onResult(movies, null, 2)
//            } else {
//                updateState(State.ERROR)
//            }
//        }
//    }

//    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Results>) {
//        updateState(State.LOADING)
//        GlobalScope.launch(Dispatchers.IO) {
//            val movies = movieAPI.getPageMovies(page = params.key + 1).results
//            if (movies.isNotEmpty()) {
//                updateState(State.DONE)
//                toMovieList(movies)
//                callback.onResult(movies, params.key + 1)
//            } else {
//                updateState(State.ERROR)
//            }
//        }
//    }
//
//    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Results>) {
//    }