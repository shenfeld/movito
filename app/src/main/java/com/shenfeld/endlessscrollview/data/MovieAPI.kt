package com.shenfeld.endlessscrollview.data

import com.shenfeld.endlessscrollview.API_KEY
import com.shenfeld.endlessscrollview.data.models.MovieResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieAPI {
    @GET("discover/movie")
    suspend fun getPageMovies(
        @Query("api_key") apiKey: String = API_KEY,
        @Query("page") page: Int,
    ): MovieResponse
}