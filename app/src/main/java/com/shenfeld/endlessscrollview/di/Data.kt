package com.shenfeld.endlessscrollview.di

import com.shenfeld.endlessscrollview.BASE_URL
import com.shenfeld.endlessscrollview.data.CustomHttpLogging
import com.shenfeld.endlessscrollview.data.MovieAPI
import com.shenfeld.endlessscrollview.data.repos.MovieRepository
import com.shenfeld.endlessscrollview.domain.repos.MovieDataSource
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val dataModule = module {
    single<MovieAPI> {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(CustomHttpLogging.getOkHttpInstance())
            .build()
            .create(MovieAPI::class.java)
    }
    single<MovieDataSource> {
        MovieRepository(movieAPI = get())
    }
}