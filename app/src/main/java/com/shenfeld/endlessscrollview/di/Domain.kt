package com.shenfeld.endlessscrollview.di

import com.shenfeld.endlessscrollview.domain.usecases.GetMoviesPageInfoUseCase
import com.shenfeld.endlessscrollview.domain.usecases_impl.GetMoviesPageInfoUseCaseImpl
import org.koin.dsl.module

val domainModule = module {
    factory<GetMoviesPageInfoUseCase> {
        GetMoviesPageInfoUseCaseImpl(movieDataSource = get())
    }
}